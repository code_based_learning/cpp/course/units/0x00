# author: a.voss@fh-aachen.de

# compiler settings
CXX = g++
# CXX = clang++
CXXFLAGS = -ansi -pedantic -Wall -Wextra -Wconversion -pthread -std=c++20
LDFLAGS = -lm

a_helloworld.out: a_helloworld.cpp
	$(CXX) $(CXXFLAGS)  a_helloworld.cpp $(LDFLAGS) -o a_helloworld.out

clean:
	rm *.out
