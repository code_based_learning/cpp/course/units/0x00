# Unit 0x00


## Compile and Run

The aim of this 'preliminary' unit is to demonstrate the compilation of a C++ program, also on the command line.

Open a terminal window and compile `a_helloworld.cpp` into a binary:
```
g++ a_helloworld.cpp -o a_helloworld.out
```

Run the program:
```
./a_helloworld.out
```

Note: If you omit the '-o' out file name, it will be named `a.out`.


## Make

In order to compile only the modified sources, `make` finds all the binaries that are older than the sources and 
starts the compilation process for them. The `makefile` contains the dependencies.

Just start:
```
make
```

## CMake

`CMake` is a cross-platform build automation software, but it is not a build system itself - it generates 
another system's build files, e.g. a `makefile`. Here it is used by `CLion` and configured in the background.
We mainly maintain the dependencies in `CMakeLists.txt`.


## German-English Confusion

In the medium term, the documentation will be available in English. At the moment I am still converting...


## Comments

Please feel free to send constructive comments and additions to [me](mailto:cpp@codebasedlearning.dev).
